# Mocha Report

This module allows developers to write frontend tests using the Mocha and Chai frameworks. These are handy for testing api's.

## Getting Started

The test script can be included in any custom module, all it has to do is set `mocha_report/mocha_report_init` as a dependency. This module will alter the library info and load those scripts in on the mocha report page.

example.libraries.yml

```
example_mocha_test:
  js:
    js/tests/mocha.v1.js: {}
    js/tests/mocha.app.js: {}

  dependencies:
    - mocha_report/mocha_report_init
 ```

mocha.vi.js

```
describe('Smell check', () => {
  it('Should be true', () => {
    assert.equal(true, true);
  });

  it('Should be able to access the homepage', (done) => {
    chai.request(requestUrl)
      .get('/')
      .end((err, response) => {
        response.should.have.status(200);
        done();
    });
  });
});
```

## Group Tests

Optionally, the tests can be grouped together to run on sub-pages. This is usefull if you have several scripts that you'd like to test individually or if you have different tests that require different user access.

example.libraries.yml

```
example_mocha_test:
  js:
    js/tests/mocha.v1.js: {}
    js/tests/mocha.app.js: {}

  dependencies:
    - mocha_report/mocha_report_init

  mocha_report:
    group: Simple tests
    description: Some tests that should be run together.

 ```
