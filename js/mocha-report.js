console.log('Check mocha-report.js for basics on writing tests.')

var assert = chai.assert;
var requestUrl = `${window.location.protocol}//${window.location.host}`;
console.log(`requestUrl is set to ${requestUrl}`);
chai.should();
chai.use(chaiHttp);
// Commented this out to avoid confusion.
// describe('Smell check', () => {
//   it('Should be true', () => {
//     assert.equal(true, true);
//   });
//
//   it('Should be able to access the homepage', (done) => {
//     chai.request(requestUrl)
//       .get('/')
//       .end((err, response) => {
//         response.should.have.status(200);
//         done();
//     });
//   });
// });
