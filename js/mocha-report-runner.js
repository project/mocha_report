// This shouldn't run on production. We can create a button here and run the
// tests on click. Tests might not be read-only.

// Optionally check drupalSettings.environmentIndicator.name to see if the
// current environment is production.

mocha.run();
