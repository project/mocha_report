<?php

namespace Drupal\mocha_report\Controller;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\mocha_report\LibraryUtilities;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Mocha Report routes.
 */
class MochaReportController extends ControllerBase {

  /**
   * The library discovery service.
   *
   * @var \Drupal\mocha_report\LibraryUtilities
   */
  protected $libraryUtilities;

  /**
   * Constructs a MochaReportController
   *
   * @param \Drupal\mocha_report\LibraryUtilities $libraryUtilities
   *   The module handler service.
   */
  public function __construct(LibraryUtilities $libraryUtilities) {
    $this->libraryUtilities = $libraryUtilities;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('mocha_report.library_utilities'),
    );
  }

  /**
   * Builds the response.
   */
  public function build() {
    $current_user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

    $groups = array_combine($this->libraryUtilities->getGroups(), array_map(function($group) {
      return $this->libraryUtilities->parseGroup($group);
    }, $this->libraryUtilities->getGroups()));

    $group_links = [];

    foreach ($groups as $pretty_group => $group) {
      $group_links[] = [
        '#markup' =>
          Link::createFromRoute($pretty_group, 'mocha_report.report.group', ['group' => $group])->toString(),
        '#suffix' => '<div class="description">' . $this->libraryUtilities->getGroupDescription($pretty_group) . '</div>',
      ];
    }

    if (empty($group_links)) {
      return [
        '#theme' => 'page__mochajs-report_page',
        '#attached' => [
          'library' => [
            'mocha_report/mocha_report_runner',
          ],
          'drupalSettings' => [
            'mochaReport' => [
              'user' => [
                'uid' => $this->currentUser()->id(),
                'name' => $this->currentUser()->getAccountName(),
                'uuid' => $current_user->get('uuid')->value,
                'isAuthenticated' => $this->currentUser()->isAuthenticated(),
              ]
            ]
          ]
        ],
      ];
    }

    return [
      '#theme' => 'item_list',
      '#items' => $group_links,
      '#attributes' => [
        'class' => ['my-link-list'],
      ],
    ];

  }

}
