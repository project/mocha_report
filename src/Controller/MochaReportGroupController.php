<?php

namespace Drupal\mocha_report\Controller;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\mocha_report\LibraryUtilities;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Mocha Report routes.
 */
class MochaReportGroupController extends ControllerBase {

  /**
   * The library discovery service.
   *
   * @var \Drupal\mocha_report\LibraryUtilities
   */
  protected $libraryUtilities;

  /**
   * Constructs a MochaReportController
   *
   * @param \Drupal\mocha_report\LibraryUtilities $libraryUtilities
   *   The module handler service.
   */
  public function __construct(LibraryUtilities $libraryUtilities) {
    $this->libraryUtilities = $libraryUtilities;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('mocha_report.library_utilities'),
    );
  }

  /**
   * Builds the response for a given group.
   */
  public function build(string $group) : array {
    $current_user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

    if (!in_array($group, array_map(function($group) { return $this->libraryUtilities->parseGroup($group); }, $this->libraryUtilities->getGroups()))) {
      throw new NotFoundHttpException();
    }

    return [
      '#theme' => 'page__mochajs-report_page',
      '#attached' => [
         'library' => [
           "mocha_report/mocha_report_runner_{$this->libraryUtilities->parseGroup($group)}",
         ],
        'drupalSettings' => [
          'mochaReport' => [
            'user' => [
              'uid' => $this->currentUser()->id(),
              'name' => $this->currentUser()->getAccountName(),
              'uuid' => $current_user->get('uuid')->value,
              'isAuthenticated' => $this->currentUser()->isAuthenticated(),
            ]
          ]
        ]
      ],
    ];
  }

  public function getTitle(string $group) : string {
    $pretty_group = $group;

    foreach ($this->libraryUtilities->getGroups() as $group_title) {
      if ($this->libraryUtilities->parseGroup($group_title) == $group) {
        $pretty_group = $group_title;
      }
    }

    return "$pretty_group | Mocha Report";
  }

}
